import json
import time
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# Create a main method to run
while True:
    try:

        # Set the hostname that we'll connect to
        parameters = pika.ConnectionParameters('rabbitmq')

        # Create a connection to rabbitmq
        connection = pika.BlockingConnection(parameters)

        # Open a channel to rabbitmq
        channel = connection.channel()

        # Create a queue if it does not exist
        channel.queue_declare(queue='presentation_approvals')

        # Create a queue if it does not exist
        channel.queue_declare(queue='presentation_rejections')

        # Print a status
        print(' [*] Waiting for messages. To exit press CTRL+C')

        def process_approval(ch, method, properties, body):
            print("  Received approval%r" % body)
            message_body = json.loads(body)
            send_mail(
                'APPROVED',
                'This presentation has been approved',
                'from@example.com',
                [message_body["presenter_email"]],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            print("  Received rejection %r" % body)
            message_body = json.loads(body)
            send_mail(
                'REJECTED',
                'This presentation has been rejected.',
                'from@example.com',
                [message_body["presenter_email"]],
                fail_silently=False,
            )
        # Configure the consumer to call the process_message function
        # when a message arrives
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        # Configure the consumer to call the process_message function
        # when a message arrives
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        # Tell rabbitmq that you're ready to receive messages
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to rabbitmq")
        time.sleep(1.0)
