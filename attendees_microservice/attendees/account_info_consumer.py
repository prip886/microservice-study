from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
import request
from attendees.models import AccountV0

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


def update_AccountV0(ch, method, properties, body):
    content = json.loads(request.body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated = datetime.fromisoformat(content["updated"])
    if is_active:
        AccountV0.objects.filter(updated__lt=updated).update_or_create(
            email=email,
            defaults={
                "first_name": first_name,
                "last_name": last_name,
                "updated": updated,
                }
            )
    else:
        AccountV0.objects.filter(email=email).delete()
            # Configure the consumer to call the process_message function
            # when a message arrives

    while True:
        try:

            parameters = pika.ConnectionParameters(host='rabbitmq')
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.exchange_declare(exchange='account_info', exchange_type='fanout')
            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange='account_info', queue=queue_name)
            channel.basic_consume(
                queue=queue_name, on_message_callback=update_AccountV0, auto_ack=True
                )
            channel.start_consuming()

        except AMQPConnectionError:
            print("Could not connect to rabbitmq")
            time.sleep(2.0)





#       Use the update_or_create method of the AccountVO.objects QuerySet
#           to update or create the AccountVO object
#   otherwise:
#       Delete the AccountVO object with the specified email, if it exists


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
#   try
#       create the pika connection parameters
#       create a blocking connection with the parameters
#       open a channel
#       declare a fanout exchange named "account_info"
#       declare a randomly-named queue
#       get the queue name of the randomly-named queue
#       bind the queue to the "account_info" exchange
#       do a basic_consume for the queue name that calls
#           function above
#       tell the channel to start consuming
#   except AMQPConnectionError
#       print that it could not connect to RabbitMQ
#       have it sleep for a couple of seconds
